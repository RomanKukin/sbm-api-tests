const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const expect = chai.expect;

chai.use(chaiHttp);

const baseUrl = 'https://dev-sbs-api.elmodev.com/';

const createAccReqBody = {
  name: `TestAcc${Date.now()}`,
  email: `test+${Date.now()}@email.com`,
  timeZone: 'Australia/Sydney',
  country: 'Australia',
  currency: 'AUD'
};

let accountID, externalKey, defaultSubscription, sessionId, subscriptionId, planObj;

const buff = new Buffer(createAccReqBody.email);
const token = buff.toString('base64');

describe('SBM Basic Flow', function () {
  this.timeout(30000);

  it('POST /accounts', function (done) {
    chai.request(baseUrl)
      .post('/accounts')
      .send(createAccReqBody)
      .end((err, res) => {
        console.log(`Create account response:${JSON.stringify(res.body, null, 4)}`);
        expect(err).to.be.null;
        res.should.have.status(201);
        res.body.should.have.property('name');
        // res.body.subscription.should.have.property('nextPaymentDate');
        // res.body.subscription.should.have.property('isTrial');
        accountID = res.body.id;
        externalKey = res.body.externalKey;
        defaultSubscription = res.body.subscription;
        return done();
      })
  });

  it('GET /accounts', function (done) {
    chai.request(baseUrl)
      .get('/accounts')
      .set('Authorization', token)
      .end((err, res) => {
        console.log(`Get account response:${JSON.stringify(res.body, null, 4)}`);
        expect(err).to.be.null;
        res.should.have.status(200);
        return done();
      })
  });

  it('POST /accounts/{id}/cards', function (done) {
    chai.request(baseUrl)
      .post(`/accounts/${accountID}/cards`)
      .set('Authorization', token)
      .end((err, res) => {
        console.log(`Add card response:${JSON.stringify(res.body, null, 4)}`);
        res.should.have.status(201);
        sessionId = res.body.sessionId;
        return done();
      })
  });

  it('POST /accounts/{id}/cards/confirm', function (done) {
    chai.request(baseUrl)
      .post(`/accounts/${accountID}/cards/confirm`)
      .set('Authorization', token)
      .send({
        'sessionId': sessionId
      })
      .end((err, res) => {
        console.log(`Confirm card response:${JSON.stringify(res.body, null, 4)}`);
        res.should.have.status(201);
        sessionId = res.body.sessionId;
        return done();
      })
  });

  it('GET /plans', function (done) {
    chai.request(baseUrl)
      .get(`/plans`)
      .set('Authorization', token)
      .end((err, res) => {
        console.log(`Get plans:${JSON.stringify(res.body, null, 4)}`);
        res.should.have.status(200);
        planObj = res.body.data.find(plan => {
          return plan.name === 'rta-monthly';
        });
        return done();
      })
  });

  it('POST /subscriptions', function (done) {
    chai.request(baseUrl)
      .post(`/subscriptions`)
      .set('Authorization', token)
      .send({
        'planName': planObj.name
      })
      .end((err, res) => {
        console.log(`Create subscription response:${JSON.stringify(res.body, null, 4)}`);
        res.should.have.status(201);
        subscriptionId = res.body.id;
        return done();
      })
  });

  it('GET /subscriptions', function (done) {
    chai.request(baseUrl)
      .get(`/subscriptions`)
      .set('Authorization', token)
      .end((err, res) => {
        console.log(`Get subscriptions response:${JSON.stringify(res.body, null, 4)}`);
        res.should.have.status(200);
        return done();
      })
  });

  it('GET /subscriptions/{id}', function (done) {
    chai.request(baseUrl)
      .get(`/subscriptions/${subscriptionId}`)
      .set('Authorization', token)
      .end((err, res) => {
        console.log(`Get subscription by Id response:${JSON.stringify(res.body, null, 4)}`);
        res.should.have.status(200);
        return done();
      })
  });

  it('GET /subscriptions/{id}/next-payment-date', function (done) {
    chai.request(baseUrl)
      .get(`/subscriptions/${subscriptionId}/next-payment-date`)
      .set('Authorization', token)
      .end((err, res) => {
        console.log(`Get next payment date response:${JSON.stringify(res.body, null, 4)}`);
        res.should.have.status(200);
        return done();
      })
  });

});